
import { useState, useContext } from 'react'
import GithubContext from '../context/github/GithubContext'
import AlertContext from '../context/alert/AlertContext' 

function UserSearch() {

    const [text, setText] = useState('') // text is part of our state so we can alter it using setText()

    // three entities which we need from our context
    const {users, searchUsers, clearUsers} = useContext(GithubContext)

    // context for the alert that occurs when someone doesn't type anything
    // into the search bar
    const {setAlert} = useContext(AlertContext) 

    // every time the textfield changes, we want to respond and grab that change
    // with the setText state
    const handleChange = (e) => setText(e.target.value) 

    const handleSubmit = (e) => {
        e.preventDefault() // prevent the webpage from refreshing

        if(text === ''){
            // since the user didn't type anything, we are sending that alert
            // with the message and tag as parameters
            setAlert('Please Enter Something', 'error') 
        } else {
            searchUsers(text) // we send the inputted text to the searchUser function in our context
            setText('')
        }
    }

    return (
        <div className="grid grid-cols-1 xl:grid-cols-2 lg:grid-cols-2 md:grid-cols-2 mb-8 gap-8">
            <div>
                <form onSubmit={handleSubmit}>
                    <div className="form-control">
                        <div className="relative">
                            <input type="text" 
                                   className="w-full pr-40 bg-gray-200 input input-lg text-black"
                                   placeholder="Search"
                                   value={text}
                                   onChange={handleChange}
                            />
                            <button type="submit" className="absolute top-0 right-0 rounded-l-none w-36 btn btn-lg">Go</button>
                        </div>
                    </div>
                </form> 
            </div>
            {users.length > 0 && (
                <div>
                    <button onClick={clearUsers} className="btn btn-ghost btn-lg">Clear</button>
                </div>
            )}
        </div>
    )
}

export default UserSearch