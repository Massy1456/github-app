

const githubReducer = (state, action) => {
    switch(action.type){
        case 'GET_USERS': 
            return {
                ...state, // we return whatever was in the state prior to this operation
                users: action.payload, // and we add the payload to users
                loading: false // turn off the spinner
            }
        
        case 'GET_USER':
            return {
                ...state,
                user: action.payload, // in this case, the payload is just a single user
                loading: false
            }
        
        case 'SET_LOADING':
            return {
                ...state,
                loading: true
            }
        case 'CLEAR_USERS':
            return {
                ...state,
                users: [], // clear the users cache
                loading: false
            }

        default: 
            return state
    }
}

export default githubReducer