import { createContext, useReducer } from 'react'
import githubReducer from './GithubReducer'

const GithubContext = createContext()

const GITHUB_URL = process.env.REACT_APP_GITHUB_URL // this the http://github.api

export const GithubProvider = ({children}) => {
    
    // this is the data that can be changed by the reducer
    // in the ./GithubReducer.js file
    const initialState = {
        users: [], 
        user: {},
        loading: false
    }

    // 'dispatch' an action to the reducer
    // our updated data will be in the 'state'
    const [state, dispatch] = useReducer(githubReducer, initialState)

    // we are searching users with a certain query
    const searchUsers = async (text) => {
        
        setLoading() // this will turn loading to True

        // turns our text that was put into the search bar into a real
        // query for the http api call
        // query=oursearchparams
        const params = new URLSearchParams({ 
            q: text
        })

        // here we actually perform the API call to Github
        const response = await fetch(`${GITHUB_URL}/search/users?${params}`, {})
        // we deconstruct the json response to get what we want, the data
        const {items} = await response.json() 
            
        // we dispatch our request with the data paylod since this is a 
        // GET request, we perform the GET reducer function
        dispatch({
            type: 'GET_USERS',
            payload: items,
        })
    }

    // Get a single user
    const getUser = async (login) => {
        
        setLoading() // set loading spinner to true

        // get the response from the github api about a single user 
        const response = await fetch(`${GITHUB_URL}/users/${login}`, {})
        
        // see if the user exists
        if(response.status === 404){
            window.location = '/notfound'
        } else {
            // return the data from the json response
            const data = await response.json()
            
            // dispatch the get user call and send the user data as payload
            // the data will hold things like username, avatar, etc
            // we are dispatching the action, hence this is action.type and action.payload
            dispatch({
                type: 'GET_USER',
                payload: data,
            })
        }

    }

    const setLoading = () => dispatch({ type: 'SET_LOADING'}) // turns on the loading spinner

    const clearUsers = () => dispatch({ type: 'CLEAR_USERS'}) // clears the users 

    // make our context span the entire front end
    // since we are using the reducer state, we need to use 'state.[object]
    return <GithubContext.Provider value={{users: state.users, user: state.user, loading:state.loading, searchUsers, getUser, clearUsers}}>
        {children}
    </GithubContext.Provider>

}

export default GithubContext