import { useReducer, createContext } from "react"
import alertReducer from './AlertReducer'

const AlertContext = createContext()

// alert used to display a message temporartily, then it disappears
// in this case, we want one to show up because the user didn't type anything
export const AlertProvider = ({children}) => {
    const initialState = null

    const [state, dispatch] = useReducer(alertReducer, initialState)

    // dispatch the alert and message
    const setAlert = (msg, tag) => {
        dispatch({
            type: 'SET_ALERT',
            payload: {msg, tag}, // sending our message and tag as payload
        })

        setTimeout(() => dispatch({ type: 'REMOVE_ALERT'}), 3000) // after 3 seconds, remove the alert
    }

    // make the alert span the entire front end
    return (<AlertContext.Provider value={{alert: state, setAlert}}>
                {children}
            </AlertContext.Provider>)
} 

export default AlertContext